import pandas as pd
import sys

def prettify_csv(file_path, separator=',', num_of_rows=10, extra_spaces=5):
    df = pd.read_csv(file_path, sep=separator, nrows=num_of_rows)

    column_widths = []

    for col in df.columns:
        width = max(len(col), df[col].astype(str).str.len().max() + extra_spaces)
        column_widths.append(width)

    # print header
    print(get_line_separator(column_widths, sep="=", col_sep="+"))
    print(get_line_with_values(df.columns, column_widths))
    print(get_line_separator(column_widths, sep="=", col_sep="+"))

    # print data
    for i in range(len(df)):
        print(get_line_with_values(df.iloc[i].values, column_widths))
        print(get_line_separator(column_widths, sep="-", col_sep="+"))


def get_line_separator(column_widths, sep="-", col_sep="+"):
    s = col_sep
    for width in column_widths:
        s += sep * width + col_sep
    return s


def get_line_with_values(values, column_widths, col_sep="|"):
    s = col_sep
    for i in range(len(column_widths)):
        s += " " * (column_widths[i] - len(str(values[i]))) + str(values[i]) + col_sep
    return s


if __name__ == '__main__':
    if len(sys.argv)<2 or len(sys.argv)>4:
        print("Incorect usage\nCorrect usage is 'python Converter.py input_csv *seperator*'")
        print("input_csv: path to the input csv file")
        print("*separator*: Delimiting character (optional)")
        exit(-1)

    path = sys.argv[2]
    separator = sys.argv[3] if sys.argv[3] else ","
    prettify_csv(path, separator)







